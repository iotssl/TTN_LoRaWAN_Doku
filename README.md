## Hier entsteht die Doku für unser TTN / LoRaWAN im Landesteil Südschleswig

Wir nutzen das [Gitlab Wiki](https://gitlab.com/iotssl/TTN_LoRaWAN_Doku/wikis/home) und können so von unsere [Homepage](https://schleswig-flensburg.freifunk.net/category/lorawan-und-iot/) oder dem [TTN Schleswig](https://www.thethingsnetwork.org/community/schleswig/) zu diesen Anleitungen linken.
